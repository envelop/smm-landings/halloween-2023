var gulp = require('gulp'),
    sass = require('gulp-sass')(require('sass')),
    browserSync = require('browser-sync').create(),
    rename = require('gulp-rename'),
    pug = require('gulp-pug'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    wait = require('gulp-wait');


gulp.task('scss', function(){
  return gulp.src('scss/styles.scss')
    .pipe(sourcemaps.init())
    .pipe(wait(1500))
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(autoprefixer({
        overrideBrowserslist: ['last 8 versions']
      }))
    .pipe(rename({suffix: '.min'}))
    .pipe(sourcemaps.write('.', {includeContent: false, sourceRoot: 'css'}))
    .pipe(gulp.dest('./public/css'))
    .pipe(browserSync.reload({stream: true}))
});

gulp.task('pug', function(){
  return gulp.src('pug/*.pug')
    .pipe(pug({
        pretty: true
    }))
    .pipe(gulp.dest('./public'))
    .pipe(browserSync.reload({stream: true}))
});

// gulp.task('libs', function () {
//   return gulp.src('js/libs/*.js')
//     .pipe(concat('libs.js'))
//     .pipe(gulp.dest('js'));
// });

gulp.task('cp_js', function () {
  return gulp.src('js/**/*')
    .pipe(gulp.dest('./public/js'));
});
gulp.task('cp_fonts', function () {
  return gulp.src('fonts/**/*')
    .pipe(gulp.dest('./public/fonts'));
});
gulp.task('cp_pics', function () {
  return gulp.src('pics/**/*')
    .pipe(gulp.dest('./public/pics'));
});

gulp.task('browser-sync', function() {
  browserSync.init({
      server: {
          baseDir: "./public/"
      }
  });
});

gulp.task('watch', function(){
  gulp.watch('scss/**/*.scss', gulp.parallel('scss'));
  gulp.watch('pug/**/*.pug', gulp.parallel('pug'));
  // gulp.watch('js/libs/*.js', gulp.parallel('libs'));
});

//gulp.task('default', gulp.parallel('scss', 'pug', 'browser-sync', 'watch'))
if (process.env.NODE_ENV === 'production') {
  gulp.task('default', gulp.parallel('scss', 'pug', 'cp_js', 'cp_fonts', 'cp_pics'));

} else {
  gulp.task('default', gulp.parallel('scss', 'pug', 'cp_js', 'cp_fonts', 'cp_pics', 'browser-sync', 'watch'));
}
