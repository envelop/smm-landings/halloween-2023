### Build with docker
```bash
docker run -it --rm  -v $PWD:/app node:16 /bin/bash -c 'cd /app && npm install && npx gulp --production'
```